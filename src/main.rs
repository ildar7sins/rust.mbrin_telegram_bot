use dotenv::var;
use telebot::Bot;
use futures::stream::Stream;

// import all available functions
use telebot::functions::*;

fn main() {
	let token = var("TELEGRAM_BOT_TOKEN").expect("TELEGRAM_BOT_TOKEN not set");

	// Create the bot
	let mut bot = Bot::new(&token).update_interval(200);

	// Register a reply command which answers a message
	let handle = bot.new_cmd("/info")
		.and_then(|(bot, msg)| {

			let mut text= match msg.from {
				None => format!("No user!"),
				Some(user)  => {
					format!("Добрый день {}. Ваш ID для сайта https://mbrin.ru - {}", user.first_name, user.id)
				}
			};

			bot.message(msg.chat.id, text.to_string()).send()
		})
		.for_each(|_| Ok(()));

	bot.run_with(handle);
}


